package hu.bs;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BsProjectApplication.class)
@WebAppConfiguration
public class BsProjectApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void isTrueTest() {
		Assert.assertTrue(true);

	}

}
