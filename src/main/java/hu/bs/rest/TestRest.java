package hu.bs.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import hu.bs.domain.Data;
import hu.bs.repository.DataRepository;

@RestController
public class TestRest {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestRest.class);

	@Autowired
	private DataRepository dataRepository;

	@RequestMapping(value = "/example", method = RequestMethod.GET)
	public Long exampleGetMethod() {
		Long count = 1L;
		return count;
	}

	@RequestMapping(value = "/exampleGet", method = RequestMethod.GET)
	public List<Data> exampleGet() {
		return dataRepository.findAll();
	}
}
