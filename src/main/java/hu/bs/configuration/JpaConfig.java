package hu.bs.configuration;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableSpringDataWebSupport
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "hu.bs.**.repository", entityManagerFactoryRef = "entityManagerFactory")
public abstract class JpaConfig {

	@Value("${jdbc.driverClassName}")
	private String driverClassName;
	@Value("${jdbc.url}")
	private String url;
	@Value("${jdbc.username}")
	private String username;
	@Value("${jdbc.password}")
	private String password;

	@Value("${hibernate.dialect}")
	private String hibernateDialect;
	@Value("${hibernate.show_sql}")
	private String hibernateShowSql;
	@Value("${hibernate.hbm2ddl.auto}")
	private String hibernateHbm2ddlAuto;

	@Value("${hibernate.connection.pool_size:10}")
	private String poolSize;

	@Value("${hibernate.connection.charSet:UTF-8}")
	private String charSet;

	@Value("${hibernate.connection.charEncodig:UTF-8}")
	private String charEncodig;

	@Value("${hibernate.connection.useUnicode:true}")
	private String useUnicode;

	@Bean()
	public DriverManagerDataSource getDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName(driverClassName);
		ds.setUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		return ds;
	}

	@Bean
	@Autowired
	public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
		return hibernateTemplate;
	}

	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(getDataSource());
		localSessionFactoryBean.setHibernateProperties(getHibernateProperties());
		localSessionFactoryBean.setPackagesToScan(new String[] { "hu.bs.**.domain" });
		localSessionFactoryBean.setAnnotatedPackages("hu.bs.**.domain");
		return localSessionFactoryBean;
	}

	@Bean
	public Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", hibernateDialect);
		properties.put("hibernate.show_sql", hibernateShowSql);
		properties.put("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);

		return properties;
	}

	@Bean
	public HibernateJpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter hjva = new HibernateJpaVendorAdapter();
		hjva.setDatabasePlatform(hibernateDialect);
		hjva.setShowSql(stringToBoolean(hibernateShowSql));
		hjva.setGenerateDdl(stringToBoolean(hibernateHbm2ddlAuto));

		return hjva;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(getDataSource());
		emf.setJpaVendorAdapter(jpaVendorAdapter());

		JpaProperties jpaProperties = new JpaProperties();
		jpaProperties.setGenerateDdl(stringToBoolean(hibernateHbm2ddlAuto));
		Properties jpaProps = new Properties();
		jpaProps.put("hibernate.hbm2ddl.auto", hibernateHbm2ddlAuto);
		jpaProps.put("hibernate.dialect", hibernateDialect);
		jpaProps.put("hibernate.connection.pool_size", poolSize);
		jpaProps.put("hibernate.connection.charSet", charSet);
		jpaProps.put("hibernate.connection.characterEncoding", charEncodig);
		jpaProps.put("hibernate.connection.useUnicode", useUnicode);
		emf.setJpaProperties(jpaProps);

		emf.setPackagesToScan("hu.bs.**.domain");
		return emf;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	private boolean stringToBoolean(String str) {
		boolean res;
		Boolean b = Boolean.valueOf(str);
		res = b.booleanValue();
		return res;
	}

}
