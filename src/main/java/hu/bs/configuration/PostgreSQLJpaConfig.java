package hu.bs.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("postgresql")
@PropertySource("classpath:/applicationProperties/postgresql.properties")
public class PostgreSQLJpaConfig extends JpaConfig {
	
}