package hu.bs.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile("mysql")
@PropertySource("classpath:/applicationProperties/mysql.properties")
public class MySQLJpaConfig extends JpaConfig {
	
}
