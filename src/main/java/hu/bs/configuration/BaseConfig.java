package hu.bs.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "hu.bs")
@PropertySource("classpath:/applicationProperties/application.properties")
public class BaseConfig {

}
