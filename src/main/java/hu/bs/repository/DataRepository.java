package hu.bs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.bs.domain.Data;

public interface DataRepository extends JpaRepository<Data, Long> {

}
