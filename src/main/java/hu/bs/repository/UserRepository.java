package hu.bs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.bs.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
